# JARD CSS framework

A flexible 12 Coloum responsive CSS framework focused for developers. It is light, fast deployment and easily configurably.
Minimised, the main CSS file is only 8.7 kB !!

The main features of JARD CSS Framework are:

- User Friendly

- SaSS Files included

- Main CSS minified is less than 9kB !!

- Fast as hell

- Easy 12 colum grid syntax

- Extendable via jQuery plugins

Easy to use, for example - if you want 3 coloums across the page:

`<div class="col4"> Your Content </div>`

`<div class="col4"> Your Content </div>`

`<div class="col4"> Your Content </div>`


The framework utilises Sass pre-proccessor and comes with all Sass files.

For full instructions see the [docs](https://jard-css.aerobatic.io)

## Changelog
 ### May 2017 Version 1.3
- Sass preproccessor now used
- Built in colors changed
- Returned to col* rather than col-*
- new boilerplate
- changed navigation
- added built in footer
- removed skyforms
- updated jquery 
- updated font awesome

